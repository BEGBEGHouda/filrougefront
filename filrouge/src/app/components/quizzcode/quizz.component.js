


const questions = [
  {
      question: "Qu'est ce qui garantit la sureté de typage dan une collection ?",
      optionA: "Les generiques",
      optionB: "Les classes abstraites",
      optionC: "Les interfaces",
      optionD: "les collections",
      correctOption: "optionB"
  },

  {
      question: "Lequels d'entre eux ne repose pas sur un index ?",
      optionA: "Les collections",
      optionB: "Les listes",
      optionC: "Les sets",
      optionD: "Les maps",
      correctOption: "optionB"
  },

  {
      question: "La collection appelée 'java.util' est ?",
      optionA: "Une classe",
      optionB: "Une interface",
      optionC: "Un objet",
      optionD: "Aucun des trois",
      correctOption: "optionA"
  },

  {
      question: "Les méthodes comme 'shuffle' ou 'reverse', peuvent etre trouvées dans ?",
      optionA: "Des objets",
      optionB: "Des collections",
      optionC: "Des dictionnaires",
      optionD: "Des listes",
      correctOption: "optionB"
  },

  {
      question: "Lequel de ces elements ne peut etre lu via un framework de collections ?",
      optionA: "les collections",
      optionB: "les groupes",
      optionC: "les listes",
      optionD: "les sets",
      correctOption: "optionB"
  },

  {
      question: "Laquelle de ces propositions autorise les éléments dupliqués ?",
      optionA: "Les sets",
      optionB: "Les listes",
      optionC: "Les deux",
      optionD: "aucun",
      correctOption: "optionB"
  },

  {
      question: " L'interface 'comparable' est appelée grace a : ?",
      optionA: "toCompare",
      optionB: "compare",
      optionC: "compareTo()",
      optionD: "compareWith",
      correctOption: "optionC"
  },

  {
      question: "Lequel de ces elements est le plus rapides (et utilise le moins de mémoire) ?",
      optionA: "Les listes d'enumeration",
      optionB: "les enumerations",
      optionC: "les itérateurs",
      optionD: "les listes d'iterateurs",
      correctOption: "optionB"
  },

  {
      question: "La capacité par defaut d'un vecteur est de ?",
      optionA: "10",
      optionB: "12",
      optionC: "8",
      optionD: "16",
      correctOption: "optionA"
  },

  {
      question: `"Lequel de ces elements gere le mieux un environement multi-threaded`,
      optionA: "WeakHashMap ",
      optionB: "Hashtable ",
      optionC: "HashMap ",
      optionD: " ConcurrentHashMap ",
      correctOption: "optionD"
  },

  {
      question: "Quel est le modificateur d’accès qui rends une méthode accessible aux meme package et sous classes de la classe?",
      optionA: "protected",
      optionB: "private",
      optionC: "public",
      optionD: "default",
      correctOption: "optionA"
  },

  {
      question: "Quelle est la plage de valeurs autorisée pour une variable déclarée avec le type byte ?",
      optionA: " [0, 255] ",
      optionB: " [0, 256] ",
      optionC: "[-128, 127] ",
      optionD: "[-127, 128] ",
      correctOption: "optionC"
  },


  {
      question: "La longueur d’une variable de type 'double' en Java est  ?",
      optionA: "Dépend du compilateur",
      optionB: "64 bits",
      optionC: "128 bits",
      optionD: "8 bits",
      correctOption: "optionB"
  },

  {
      question: " Lequel des éléments suivants n’est pas un concept POO en Java ?",
      optionA: "Heritage",
      optionB: "Encapsulation",
      optionC: "Polymorphisme",
      optionD: "Compilation",
      correctOption: "optionD"
  },

  {
      question: "Quand la surcharge de méthode est-elle déterminée ?",
      optionA: "l'execution",
      optionB: "la compilation",
      optionC: "le codage",
      optionD: "La pre compilation ",
      correctOption: "optionB"
  },

  {
      question: "Quel concept de Java est un moyen de convertir des objets du monde réel en termes de classe ?",
      optionA: "Polymorphisme",
      optionB: "Encapsulation",
      optionC: "Abstraction",
      optionD: "Heritage",
      correctOption: "optionC"
  },

  {
      question: "Quel concept de Java est utilisé en combinant des méthodes et des attributs dans une classe?",
      optionA: "Encapsulation",
      optionB: "Polymorphisme",
      optionC: "Heritage",
      optionD: "Abstraction",
      correctOption: "optionA"
  },

  {
      question: "Comment s’appelle le cas où l’objet d’une classe mère est détruit donc l’objet d’une classe fille l'est également?",
      optionA: "Agrégation",
      optionB: "Composition",
      optionC: "Encapsulation",
      optionD: "Association",
      correctOption: "optionB"
  },

  {
      question: "Quel environnement de développement est proposé par Java ? ?",
      optionA: "JVM",
      optionB: "JSD",
      optionC: "JDK",
      optionD: "JRE",
      correctOption: "optionC"
  },

  {
      question: "Quelle méthode renvoie les éléments de la classe Enum?",
      optionA: "getEnum()",
      optionB: "getEnumList()",
      optionC: "getEnums()",
      optionD: "getEnumConstants()",
      correctOption: "optionA"
  },

  {
      question: "Quel est l’ordre des variables dans Enum ?",
      optionA: "Ordre décroissant",
      optionB: "Ordre croissant",
      optionC: "Ordre aleatoire",
      optionD: "Depend de la méthode order()",
      correctOption: "optionB"
  },

  {
      question: "Lequel des éléments suivants gère une liste de pilotes de base de données dans JDBC? ?",
      optionA: "DriverManager",
      optionB: "JDBC driver",
      optionC: "Connection",
      optionD: "Statement",
      correctOption: "optionA"
  },

  {
      question: "Lequel des énoncés suivants est plus efficace que Statement en raison de la précompilation de SQL???",
      optionA: "Statement",
      optionB: "PreparedStatement",
      optionC: "CallableStatement",
      optionD: "Aucune des propositions",
      correctOption: "optionB"
  },

  {
      question: "Parmis les itérations, laquelle va permettre de parcourir la collection dans chaque direction ?",
      optionA: "Iterator",
      optionB: "ListIterator",
      optionC: "Checkiterator",
      optionD: "Mapiterator",
      correctOption: "optionB"
  },

  {
      question: "Laquelle de ces categories a la capacité de croitre de maniere dynamique?",
      optionA: "Array",
      optionB: "Arrays",
      optionC: "ArrayList",
      optionD: "Aucunes d'entre eux",
      correctOption: "optionC"
  }

]


let shuffledQuestions = [] //empty array to hold shuffled selected questions out of all available questions

function handleQuestions() {
  //function to shuffle and push 10 questions to shuffledQuestions array
//app would be dealing with 10questions per session
  while (shuffledQuestions.length <= 9) {
      const random = questions[Math.floor(Math.random() * questions.length)]
      if (!shuffledQuestions.includes(random)) {
          shuffledQuestions.push(random)
      }
  }
}


let questionNumber = 1 //holds the current question number
let playerScore = 0  //holds the player score
let wrongAttempt = 0 //amount of wrong answers picked by player
let indexNumber = 0 //will be used in displaying next question

// function for displaying next question in the array to dom
//also handles displaying players and quiz information to dom
function NextQuestion(index) {
  handleQuestions()
  const currentQuestion = shuffledQuestions[index]
  document.getElementById("question-number").innerHTML = questionNumber
  document.getElementById("player-score").innerHTML = playerScore
  document.getElementById("display-question").innerHTML = currentQuestion.question;
  document.getElementById("option-one-label").innerHTML = currentQuestion.optionA;
  document.getElementById("option-two-label").innerHTML = currentQuestion.optionB;
  document.getElementById("option-three-label").innerHTML = currentQuestion.optionC;
  document.getElementById("option-four-label").innerHTML = currentQuestion.optionD;

}


function checkForAnswer() {
  const currentQuestion = shuffledQuestions[indexNumber] //gets current Question
  const currentQuestionAnswer = currentQuestion.correctOption //gets current Question's answer
  const options = document.getElementsByName("option"); //gets all elements in dom with name of 'option' (in this the radio inputs)
  let correctOption = null

  options.forEach((option) => {
      if (option.value === currentQuestionAnswer) {
          //get's correct's radio input with correct answer
          correctOption = option.labels[0].id
      }
  })

  //checking to make sure a radio input has been checked or an option being chosen
  if (options[0].checked === false && options[1].checked === false && options[2].checked === false && options[3].checked == false) {
      document.getElementById('option-modal').style.display = "flex"
  }

  //checking if checked radio button is same as answer
  options.forEach((option) => {
      if (option.checked === true && option.value === currentQuestionAnswer) {
          document.getElementById(correctOption).style.backgroundColor = "green"
          playerScore++ //adding to player's score
          indexNumber++ //adding 1 to index so has to display next question..
          //set to delay question number till when next question loads
          setTimeout(() => {
              questionNumber++
          }, 1000)
      }

      else if (option.checked && option.value !== currentQuestionAnswer) {
          const wrongLabelId = option.labels[0].id
          document.getElementById(wrongLabelId).style.backgroundColor = "red"
          document.getElementById(correctOption).style.backgroundColor = "green"
          wrongAttempt++ //adds 1 to wrong attempts
          indexNumber++
          //set to delay question number till when next question loads
          setTimeout(() => {
              questionNumber++
          }, 1000)
      }
  })
}



//called when the next button is called
function handleNextQuestion() {
  checkForAnswer() //check if player picked right or wrong option
  unCheckRadioButtons()
  //delays next question displaying for a second just for some effects so questions don't rush in on player
  setTimeout(() => {
      if (indexNumber <= 9) {
//displays next question as long as index number isn't greater than 9, remember index number starts from 0, so index 9 is question 10
          NextQuestion(indexNumber)
      }
      else {
          handleEndGame()//ends game if index number greater than 9 meaning we're already at the 10th question
      }
      resetOptionBackground()
  }, 1000);
}

//sets options background back to null after display the right/wrong colors
function resetOptionBackground() {
  const options = document.getElementsByName("option");
  options.forEach((option) => {
      document.getElementById(option.labels[0].id).style.backgroundColor = ""
  })
}

// unchecking all radio buttons for next question(can be done with map or foreach loop also)
function unCheckRadioButtons() {
  const options = document.getElementsByName("option");
  for (let i = 0; i < options.length; i++) {
      options[i].checked = false;
  }
}

// function for when all questions being answered
function handleEndGame() {
  let remark = null
  let remarkColor = null

  // condition check for player remark and remark color
  if (playerScore <= 3) {
      remark = "Hmm vous avez encore besoin d'entrainement."
      remarkColor = "red"
  }
  else if (playerScore >= 4 && playerScore < 7) {
      remark = "C'est pas mal, mais vous pouvez vous ameliorer encore!!"
      remarkColor = "orange"
  }
  else if (playerScore >= 7) {
      remark = "Excellent, continuez comme ça!!"
      remarkColor = "green"
  }
  const playerGrade = (playerScore / 10) * 100

  //data to display to score board
  document.getElementById('remarks').innerHTML = remark
  document.getElementById('remarks').style.color = remarkColor
  document.getElementById('grade-percentage').innerHTML = playerGrade
  document.getElementById('wrong-answers').innerHTML = wrongAttempt
  document.getElementById('right-answers').innerHTML = playerScore
  document.getElementById('score-modal').style.display = "flex"

}

//closes score modal, resets game and reshuffles questions
function closeScoreModal() {
  questionNumber = 1
  playerScore = 0
  wrongAttempt = 0
  indexNumber = 0
  shuffledQuestions = []
  NextQuestion(indexNumber)
  document.getElementById('score-modal').style.display = "none"
}

//function to close warning modal
function closeOptionModal() {
  document.getElementById('option-modal').style.display = "none"
}
