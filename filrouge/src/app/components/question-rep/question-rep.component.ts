import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Question } from 'src/app/models/question/question.model';
import { Reponse } from 'src/app/models/reponse/reponse.model';
import { QuestionRepService } from 'src/app/services/question-rep.service';

@Component({
  selector: 'app-question-rep',
  templateUrl: './question-rep.component.html',
  styleUrls: ['./question-rep.component.scss']
})
export class QuestionRepComponent implements OnInit {
  question: Question = new Question();


  constructor(private questionRepService: QuestionRepService) { }

  ngOnInit(): void {
    console.log(this.question);
    // Si on a un id dans l'url => édition et pas ajout
    // DONC On va faire un get avec l'id pour récupérer les informations de la question
    // Puis initialiser le formulaire avec ces données (this.question)
    // Si tu récupères que 2 éléments dans question.reponses du back
    // questionDuBack = la question récupérée dans le subscribe (grâce au service)
    // const question = { ...questionDuBack };
    // for (let i = question.reponses.length(); i < 4; i++) {
    //   question.reponses.push(new Reponse());
    // }
    // this.question = question;
  }

  onSubmit(form: NgForm): void{
    console.log(this.question);
    // Pas utile ici
    // const question = new Question();
    // question.technologie = form.value['techno'];
    // question.difficulte = form.value['difficulte'];
    // question.question = form.value['question'];
    // question.reponses = [];
    // if (form.value['reponse1']) {
    //   question.reponses.push({ reponse: form.value['reponse1'], correcte: form.value['correct1'] });
    // }
    const questionToSend = { ...this.question };
    questionToSend.reponses = questionToSend.reponses.filter(o => o.reponse);
    console.log(questionToSend);

    // Si id dans l'url : Modification DONC this.questionRepService.changeQuestion
    // Sinon 
    // this.questionRepService.saveQuestion(questionToSend).subscribe(
    //   ()=>{
    //     console.log('Question créé');
    //   },
    //   (error) => {
    //     console.log('erreur : ' + error);
    //   }
    // );
  }

}
