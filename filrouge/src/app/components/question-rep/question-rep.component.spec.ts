import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionRepComponent } from './question-rep.component';

describe('QuestionRepComponent', () => {
  let component: QuestionRepComponent;
  let fixture: ComponentFixture<QuestionRepComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuestionRepComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionRepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
