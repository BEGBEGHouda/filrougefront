import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Question } from '../models/question/question.model';

@Injectable({
  providedIn: 'root'
})
export class QuestionRepService {

  constructor(private http: HttpClient) { }

  private url: string = 'http://localhost:8080'

  // getQuestion(id): Observable<Question> {
  //   return this.http.get<Question>(...)
  // }

  saveQuestion(question: Question): Observable<any> {
    return this.http.post<any>(`${this.url}/question`, question);

  }
}
