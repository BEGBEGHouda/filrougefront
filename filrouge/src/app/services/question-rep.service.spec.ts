import { TestBed } from '@angular/core/testing';

import { QuestionRepService } from './question-rep.service';

describe('QuestionRepService', () => {
  let service: QuestionRepService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QuestionRepService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
