import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './components/admin/admin.component';
import { ConnexionComponent } from './components/connexion/connexion.component';
import { QuestionRepComponent } from './components/question-rep/question-rep.component';
// import { AuthGuard } from './auth.guard';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'connexion'},
  { path: 'connexion', component: ConnexionComponent },
  { path: 'admin', component: AdminComponent },
  {path: 'question', component: QuestionRepComponent},
  {path: 'question/:id', component: QuestionRepComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
