import { Reponse } from "../reponse/reponse.model";

export class Question { 
  technologie: string = "";
  difficulte: string = "";
  question: string = "";
  commentaire: string = "";
  reponses: Reponse[] = [];

  constructor() {
    this.reponses = [
      new Reponse(),
      new Reponse(),
      new Reponse(),
      new Reponse()
    ]
  }

}
